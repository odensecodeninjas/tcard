﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelCard
{
    class TravelCard
    {
        static Random rnd = new Random();
        private int saldo = 0;
        List<User> users = new List<User>();

        public CustomerType CustomerType { get; set; }
        public double Saldo { get; set; }
        public int Type { get; set; }

        public int Id { get; set; }

        public void AddUser(User user)
        {
            users.Add(user);
        }
        public List<User> GetUser()
        {
            return users;
        }
        public double setCurrentSaldo(double saldo)
        {
            return saldo;
        }
        public double getCurrentSaldo(double price)
        {
            double newSaldo = (saldo - price);
            return newSaldo;
        }
    }
}
