﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelCard
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<TravelRegistration> travels = new List<TravelRegistration>();            
         
            bool keepGoing = true;
            bool userHasExtras = false;
                  
            int choice;

            while (keepGoing)
            {
                choice = showMenu();
                switch (choice)
                {
                    case 0:
                        keepGoing = false;
                        break;
                    case 1:
                        userHasExtras = false;                       
                        TrainTripStart(travels, userHasExtras);
                        break;
                    case 2:
                        userHasExtras = true;
                        TrainTripStart(travels, userHasExtras);
                        break;
                    default:
                        Console.WriteLine("That was not a valid input");
                        break;
                } // end switch                
            } // end while loop            
            Console.ReadLine();
        }              

        public static void TrainTripStart(List<TravelRegistration> travels, bool extraOrNot)
        {
            TravelRegistration travel = new TravelRegistration();

            Station ST = new Station();

            if (extraOrNot)
            {
                AddExtras(travel);
            }

            travel.Card = new TravelCard();
            travel.Card.Id = 1;

            if(travel.Card.Saldo == 0)
            {
                travel.Card.Saldo = 5000;
            }

            User user = new User();
            user.Id = 1;
            user.Name = "Kim Petersen";            
            travel.Card.AddUser(user);

            ST.zoneCounter = 0;
            ST.RandomRoute();                 
        
            travel.ZoneTotal = ST.zoneCounter;
            Console.WriteLine("The train has stopped.");
            Console.WriteLine(" \nYou have arrived at " + ST.stopStation);

            travels.Add(travel);
            TrainTripEnd(travel);

        }

        public static void TrainTripEnd(TravelRegistration travel)
        {
            double price = travel.tripPrice();
            double newSaldo = (travel.Card.Saldo - price - travel.getExtraCost());

            travel.Card.Saldo = newSaldo;

            Console.WriteLine("You have traveled: " + travel.ZoneTotal + " zones");
            Console.WriteLine("The cost of your trip is:" + travel.tripPrice());
            Console.WriteLine("The cost of your extras is:" + travel.getExtraCost());
            Console.WriteLine("Your current saldo is:" + travel.Card.Saldo);

            Console.WriteLine("Press a key to travel again.");
            Console.ReadKey(true);
        }

        private static void AddExtras(TravelRegistration travels)
        {
            int number = 0;
            string inputValue = "";

            //Console.Clear();
            Console.WriteLine("Select what kind of extra to add.");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("0) Abort - Starting the train with NO extras.");
            Console.WriteLine("1) Extra Passenger.");
            Console.WriteLine("2) Bicycle.");
            Console.WriteLine("3) Baby carriage.");

            inputValue = Console.ReadLine();

            bool result = Int32.TryParse(inputValue, out number);

            if (result)
            {

                if (number >= 1 && number <= 3)
                {
                    Extras extra = new Extras();

                    Console.WriteLine("Write amount of extras...");
                    extra.Amount = int.Parse(Console.ReadLine());

                    string name = getExtraName(number);
                    extra.Name = name;
                    travels.calcExtras(number, extra.Amount);
                    travels.AddExtras(extra);
                }
                else if(number == 0)
                {
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Wrong number! Press a key to try again!");
                    Console.ReadKey();
                    AddExtras(travels);
                }
            }
            else
            {
                Console.WriteLine("You have to write a number! Press a key to try again!");
                Console.ReadKey();
                AddExtras(travels);
            }
        }

        private static string getExtraName(int number)
        {
            if(number == 1)
            {
                return "Extra Passenger";
            }
            else if(number == 2)
            {
                return "Bicycle";
            }
            else
            {
                return "Baby carriage";
            }
        }

        static int showMenu()
        {

            int input = 1;
            Console.WriteLine("Arriving at station - scanner catches RFID");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("0) Leave station. Scanner clears RFID registration.");
            Console.WriteLine("1) Enter the transportation alone, no extras.");
            Console.WriteLine("2) Enter the transportation, with extras.");            

            try
            {
                input = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect input");
                input = 1;
            } // end try
            return input;
        } // end showMenu     
    }
}
