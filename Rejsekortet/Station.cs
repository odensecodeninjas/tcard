﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelCard
{
    class Station
    {
        public List<string> StationListe = new List<string>();
        public int zoneCounter = 0;
        public int stationID = 0;
        public string stopStation = "error";
        public Transport Transport { get; set; }
        Transport myTravel = new Transport();       

        private int transportType;
        private string transport;
        //private string name;
        //private string address;

        public int TransportType
            {
            get
            {
                return transportType;
            } // end get
            set
            {
                transportType = value;
                if (transportType == 1)
                {
                    transport = "Train Only";
                    myTravel.Type = transport;
                    //Console.WriteLine("Transporttype consist of: {0}", myTravel.Type);
                }
                else if (transportType == 2)
                {
                    transport = "Bus Only";
                    myTravel.Type = transport;
                    //Console.WriteLine("Transporttype consist of: {0}", myTravel.Type);
                }
                else
                {
                    transport = "Train and Bus";
                    myTravel.Type = transport;
                    //Console.WriteLine("Transporttype consist of: {0}", myTravel.Type);
                } // end if
            }
        }

        public string Name { get; set; }      
        public string Address { get; set; }
        public void RandomRoute()
        {
            Random rnd = new Random();
            FillStationList();
            int startStation = rnd.Next(StationListe.Count);
            int WestOrEast = rnd.Next(1, 3);

            Console.WriteLine("You're at " + StationListe[startStation]);
            stationID = startStation;
            if (stationID == 0)
            {
                WestOrEast = 1;
            }
            else if (stationID == StationListe.Count)
            {
                WestOrEast = 2;
            }
            if (WestOrEast == 1)
            {
                Console.WriteLine("The train is heading west towards Lindholm Station");
                ZoneGoingWest();
            }
            else
            {
                Console.WriteLine("The train is heading east towards København Hovedbanegård");
                ZoneGoingEast();
            }
        }
        public void ZoneGoingWest()
        {
            Console.WriteLine("Choo Choo! Press a key when you want to stop!");
            int startStation = stationID;
            while (stationID < StationListe.Count - 1)
            {
                Console.WriteLine(".");
                Console.WriteLine("Next station is " + StationListe[stationID + 1]);

                zoneCounter++;
                stationID++;
                System.Threading.Thread.Sleep(1000);
                if (stationID == StationListe.Count - 1)
                {
                    Console.WriteLine("The train doesn't go any further");
                    stopStation = StationListe[stationID];
                    stationID = StationListe.Count;
                }
                if (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                    stopStation = StationListe[stationID];
                    stationID = StationListe.Count;
                }
            }
            //Console.WriteLine("\nThe train doesn't go any further");
            //Console.WriteLine("Last stop is " + StationListe[stationID]);
        }
        public void ZoneGoingEast()
        {
            Console.WriteLine("Choo Choo! Press a key when you want to stop!");
            while (stationID > 0)
            {
                Console.WriteLine(".");
                Console.WriteLine("Next station is " + StationListe[stationID - 1]);
                
                zoneCounter++;
                stationID--;
                System.Threading.Thread.Sleep(1000);
                if (stationID == 0)
                {
                    Console.WriteLine("\nThe train doesn't go any further");
                    stopStation = StationListe[stationID];
                    stationID = 0;
                }
                if (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                    stopStation = StationListe[stationID];
                    stationID = 0;
                }
            }
            //Console.WriteLine("Last stop is " + StationListe[0]);
        }
        public void FillStationList()
        {

            StationListe.Add("København Hovedbanegård");
            StationListe.Add("Højtaastrup Station");
            StationListe.Add("Roskilde Station");
            StationListe.Add("Ringsted Station");
            StationListe.Add("Sorø Station");
            StationListe.Add("Slagelse Station");
            StationListe.Add("Korsør Station");
            StationListe.Add("Nyborg Station");
            StationListe.Add("Odense Hovedbanegård");
            StationListe.Add("Middelfart Station");
            StationListe.Add("Fredericia Station");
            StationListe.Add("Vejle Station");
            StationListe.Add("Horsens Station");
            StationListe.Add("Skanderborg Station");
            StationListe.Add("Aarhus Hovedbanegård");
            StationListe.Add("Hadsten Station");
            StationListe.Add("Langå Station");
            StationListe.Add("Randers Station");
            StationListe.Add("Hobro Station");
            StationListe.Add("Arden Station");
            StationListe.Add("Skørping Station");
            StationListe.Add("Støvring Station");
            StationListe.Add("Svendstrup Station");
            StationListe.Add("Skalborg Station");
            StationListe.Add("Aalborg Hovedbanegård");
            StationListe.Add("Aalborg Vestby Station");
            StationListe.Add("Lindholm Station");
        }
    }
}
