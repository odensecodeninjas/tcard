﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelCard
{
    class TravelRegistration
    {
        //private double saldo;
        //private double zonetotal = 0.0;

        List<Station> stations = new List<Station>();
        List<Extras> extras = new List<Extras>();

        private int id;
        private int extracost;
        private int typecost;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public int PersonID { get; set; }
        public int ZoneTotal { get; set; }

        public TravelCard Card { get; set; }

        public User UserInfo { get; set; }

        public void AddStation(Station station)
        {
            stations.Add(station);
        }

        public List<Station> GetStations()
        {
            return stations;
        }

        public void AddExtras(Extras extra)
        {
            extras.Add(extra);
        }

        public List<Extras> GetExtras()
        {
            return extras;
        }

        public double tripPrice()
        {
            TravelCard ct = new TravelCard();
            TravelRegistration travel = new TravelRegistration();

            double old = 0.9; // 10% rabbat
            double young = 0.8; // 20% rabbat
            double zoneCost = 50;
            double discount = 1;

            if (ct.Type == old)
            {
                discount = old;
            }
            else if (ct.Type == young)
            {
                discount = young;
            }
            else
            {
                discount = 1;
            }
            double tripPrice = zoneCost * ZoneTotal * discount;
            return tripPrice;
        }
        
       public void calcExtras(int type, int amount)
        {
            if(type == 1)
            {
                typecost = 50;
            }
            else if(type == 2)
            {
                typecost = 25;
            }
            else if(type == 3)
            {
                typecost = 75;
            }
            extracost = (typecost * amount);
        }
        public int getExtraCost()
        {
            return extracost * ZoneTotal;
        }
        
    }

    
}
