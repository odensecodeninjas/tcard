﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelCard
{
    class User
    {
        private int id;
        //private int age;
        //private string education;
        //private string group;
        //private string job;
        //private string name;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public int Age { get; set; }
        public string Education { get; set; }
        public string Job { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }

    }
}
